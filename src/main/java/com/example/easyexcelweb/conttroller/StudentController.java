package com.example.easyexcelweb.conttroller;

import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.event.AnalysisEventListener;
import com.example.easyexcelweb.domain.Student;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Controller
public class StudentController {
    @RequestMapping("students")
    public static List<Student> Data() {
        List<Student> students = new ArrayList<Student>();
        Student data = new Student();
        for (int i = 0; i < 10; i++) {
            data.setName("学生00" + i);
            data.setGender("男");
            data.setBirthday(new Date());
            students.add(data);
        }
        //model.addAttribute("map",students);

        return students;
    }

    @GetMapping("export")
    public void exPort(HttpServletResponse response) throws Exception {
        String fileName = "test";
        response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        response.setCharacterEncoding("utf-8");
        response.setHeader("Content-disposition", "attachment;filename*=utf-8''" + fileName + ".xlsx");
        EasyExcel.write(response.getOutputStream(), StudentController.class).sheet("学生").doWrite(Data());
    }

    @RequestMapping ("/imPort")
    public String imPort(Map map1){
        EasyExcel.read("Student.xlsx", Student.class, new AnalysisEventListener<Student>() {
            @Override
            public void invoke(Student student, AnalysisContext analysisContext) {
                System.out.println("student " + student);
            }

            @Override
            public void doAfterAllAnalysed(AnalysisContext analysisContext) {
                System.out.println("读取完毕");
            }
        }).sheet().doRead();
//        ExcelReaderBuilder read = EasyExcel.read("Student.xlsx", Student.class, new StudentListener<Student>());
//        ExcelReaderSheetBuilder sheet = read.sheet();
//        sheet.doRead();
        return "imPort";
    }


    /*public class StudentListener extends AnalysisEventListener<Student> {
        List list = new ArrayList();
        //每读一行内容，都会调用一次invoke，在invoke可以操作使用读取到的数据
        @Override
        public void invoke(Student student, AnalysisContext analysisContext) {
            System.out.println("student " + student);
            list.add(student);
        }

        //读取完整个文档后调用的方法
        @Override
        public void doAfterAllAnalysed(AnalysisContext analysisContext) {
            System.out.println("读取完毕");

        }
    }*/
}
