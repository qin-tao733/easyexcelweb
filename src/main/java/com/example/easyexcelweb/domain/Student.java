package com.example.easyexcelweb.domain;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.format.DateTimeFormat;
import com.alibaba.excel.annotation.write.style.ColumnWidth;
import com.alibaba.excel.annotation.write.style.ContentRowHeight;
import com.alibaba.excel.annotation.write.style.HeadRowHeight;
import jdk.nashorn.internal.ir.annotations.Ignore;
import lombok.Data;

import java.util.Date;

@Data
//@HeadRowHeight:设置表头行高
//@ContentRowHeight：设置内容行高
//@ExcelProperty(index = ),index可以指定当前字段位于excel表中那一列
public class Student {
    @ExcelProperty("ID")
    @Ignore//忽略一个属性
    private String id;
    @ExcelProperty("学生姓名")
    @ColumnWidth(20)//调整宽度
    private String name;
    @ExcelProperty("学生性别")
    @ColumnWidth(20)
    private String gender;
    @ExcelProperty("出生日期")
    @ColumnWidth(20)
    @DateTimeFormat("yyyy-MM-dd")//指定日期格式
    private Date birthday;
}
