package com.example.easyexcelweb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EasyexcelwebApplication {

    public static void main(String[] args) {
        SpringApplication.run(EasyexcelwebApplication.class, args);
    }

}
